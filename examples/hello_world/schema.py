from wattle import load_schema
from wattle.nodes import Nested, Value, Boolean


class Test:
    test = Boolean()


class MessagePrinter:
    indent = Value(int)
    capitalize = Value(bool)
    test = Nested(Test)

    def print(self, message):
        if self.indent:
            message = (" " * self.indent) + message
        if self.capitalize:
            message = message.upper()
        print(message)


class HelloWorld:
    message = Value(str)
    how_many_times = Value(int, default=25)
    printer = Nested(MessagePrinter)

    def print(self):
        for _ in range(self.how_many_times):
            self.printer.print(self.message)


if __name__ == '__main__':
    schema = load_schema(HelloWorld)
    root = schema.read('examples/hello_world/input.yml')
    root.print()
    print(root)
